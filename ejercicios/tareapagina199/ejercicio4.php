<?php
  $numero = isset($_POST['numero']) ? $_POST['numero'] : '';

  $contador = 0;

  $cadena = strval($numero);

  for ($i = 0; $i < strlen($cadena); $i++) {
    if ($cadena[$i] == '0') {
      $contador++;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Contar dígitos "0" en un número</title>
</head>
<body>
  <h1>Contar dígitos "0" en un número</h1>
  <form method="post">
    <label for="numero">Ingrese un número:</label>
    <input type="number" name="numero" id="numero" value="<?php echo $numero; ?>">
    <button type="submit">Contar</button>
  </form>
  
  <?php if ($numero != '') { ?>
    <p>El número <?php echo $numero; ?> contiene <?php echo $contador; ?> dígitos "0".</p>
  <?php } ?>
  
</body>
</html>
