<?php
  $n = isset($_POST['n']) ? $_POST['n'] : '';

  $suma = 0;
  $producto = 1;
  $contador = 0;

  for ($i = 3; $contador < $n; $i += 3) {
    $suma += $i;
    $producto *= $i;
    $contador++;
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Suma y producto de los N primeros múltiplos de 3</title>
</head>
<body>
  <h1>Suma y producto de los N primeros múltiplos de 3</h1>
  <form method="post">
    <label for="n">Ingrese la cantidad de números:</label>
    <input type="number" name="n" id="n" value="<?php echo $n; ?>">
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($n != '') { ?>
    <p>Los primeros <?php echo $n; ?> números naturales múltiplos de 3 son:</p>
    <ul>
      <?php for ($i = 3; $contador < $n; $i += 3) { ?>
        <li><?php echo $i; ?></li>
        <?php $contador++; ?>
      <?php } ?>
    </ul>
    <p>La suma de los primeros <?php echo $n; ?> números naturales múltiplos de 3 es: <?php echo $suma; ?></p>
    <p>El producto de los primeros <?php echo $n; ?> números naturales múltiplos de 3 es: <?php echo $producto; ?></p>
  <?php } ?>
  
</body>
</html>
