<?php
  $inicio = isset($_POST['inicio']) ? $_POST['inicio'] : '';
  $fin = isset($_POST['fin']) ? $_POST['fin'] : '';

  function es_primo($numero) {
    if ($numero <= 1) {
      return false;
    }
    for ($i = 2; $i <= $numero / 2; $i++) {
      if ($numero % $i == 0) {
        return false;
      }
    }
    return true;
  }

  $primos = 0;

  for ($i = $inicio; $i <= $fin; $i++) {
    if (es_primo($i)) {
      $primos++;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Cantidad de números primos en un rango</title>
</head>
<body>
  <h1>Cantidad de números primos en un rango</h1>
  <form method="post">
    <label for="inicio">Ingrese el inicio del rango:</label>
    <input type="number" name="inicio" id="inicio" value="<?php echo $inicio; ?>"><br>
    <label for="fin">Ingrese el fin del rango:</label>
    <input type="number" name="fin" id="fin" value="<?php echo $fin; ?>"><br>
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($inicio != '' && $fin != '') { ?>
    <p>El rango de <?php echo $inicio; ?> a <?php echo $fin; ?> contiene <?php echo $primos; ?> números primos.</p>
  <?php } ?>
  
</body>
</html>
