<?php
  $numero = isset($_POST['numero']) ? $_POST['numero'] : '';

  $cadena = strval($numero);

  $pares = 0;
  $impares = 0;
  $neutros = 0;

  for ($i = 0; $i < strlen($cadena); $i++) {
    if ($cadena[$i] == '0') {
      $neutros++;
    } elseif ($cadena[$i] % 2 == 0) {
      $pares++;
    } else {
      $impares++;
    }
  }

  $total = $pares + $impares + $neutros;
  $porcentaje_pares = ($total > 0) ? round($pares / $total * 100, 2) : 0;
  $porcentaje_impares = ($total > 0) ? round($impares / $total * 100, 2) : 0;
  $porcentaje_neutros = ($total > 0) ? round($neutros / $total * 100, 2) : 0;
?>

<!DOCTYPE html>
<html>
<head>
  <title>Porcentaje de números pares, impares y neutros en un número</title>
</head>
<body>
  <h1>Porcentaje de números pares, impares y neutros en un número</h1>
  <form method="post">
    <label for="numero">Ingrese un número:</label>
    <input type="number" name="numero" id="numero" value="<?php echo $numero; ?>"><br>
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($numero != '') { ?>
    <p>El número <?php echo $numero; ?> contiene:</p>
    <ul>
      <li><?php echo $pares; ?> números pares (<?php echo $porcentaje_pares; ?>%)</li>
      <li><?php echo $impares; ?> números impares (<?php echo $porcentaje_impares; ?>%)</li>
      <li><?php echo $neutros; ?> números neutros (<?php echo $porcentaje_neutros; ?>%)</li>
    </ul>
  <?php } ?>
  
</body>
</html>
