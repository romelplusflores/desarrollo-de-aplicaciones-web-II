<?php
  $numero1 = isset($_POST['numero1']) ? $_POST['numero1'] : '';
  $numero2 = isset($_POST['numero2']) ? $_POST['numero2'] : '';

  function mcd_euclides($a, $b) {
    while ($b != 0) {
      $t = $b;
      $b = $a % $b;
      $a = $t;
    }
    return $a;
  }

  $mcd = mcd_euclides($numero1, $numero2);
?>

<!DOCTYPE html>
<html>
<head>
  <title>Cálculo del MCD de dos números</title>
</head>
<body>
  <h1>Cálculo del MCD de dos números</h1>
  <form method="post">
    <label for="numero1">Ingrese el primer número:</label>
    <input type="number" name="numero1" id="numero1" value="<?php echo $numero1; ?>"><br>
    <label for="numero2">Ingrese el segundo número:</label>
    <input type="number" name="numero2" id="numero2" value="<?php echo $numero2; ?>"><br>
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($numero1 != '' && $numero2 != '') { ?>
    <p>El MCD de <?php echo $numero1; ?> y <?php echo $numero2; ?> es <?php echo $mcd; ?>.</p>
  <?php } ?>
  
</body>
</html>
