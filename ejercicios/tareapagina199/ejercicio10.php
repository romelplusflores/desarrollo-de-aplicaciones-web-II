<?php
  $numero1 = isset($_POST['numero1']) ? $_POST['numero1'] : '';
  $numero2 = isset($_POST['numero2']) ? $_POST['numero2'] : '';

  function factores_primos($numero) {
    $factores = array();
    $divisor = 2;
    while ($numero > 1) {
      while ($numero % $divisor == 0) {
        $factores[] = $divisor;
        $numero /= $divisor;
      }
      $divisor++;
    }
    return $factores;
  }

  $factores1 = factores_primos($numero1);
  $factores2 = factores_primos($numero2);

  $factores_comunes = array_intersect($factores1, $factores2);
  $factores_no_comunes = array_merge(array_diff($factores1, $factores_comunes), array_diff($factores2, $factores_comunes));

  $mcd = 1;
  foreach ($factores_comunes as $factor) {
    $exponente1 = count(array_keys($factores1, $factor));
    $exponente2 = count(array_keys($factores2, $factor));
    $mcd *= pow($factor, min($exponente1, $exponente2));
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Cálculo del MCD de dos números</title>
</head>
<body>
  <h1>Cálculo del MCD de dos números</h1>
  <form method="post">
    <label for="numero1">Ingrese el primer número:</label>
    <input type="number" name="numero1" id="numero1" value="<?php echo $numero1; ?>"><br>
    <label for="numero2">Ingrese el segundo número:</label>
    <input type="number" name="numero2" id="numero2" value="<?php echo $numero2; ?>"><br>
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($numero1 != '' && $numero2 != '') { ?>
    <p>El MCD de <?php echo $numero1; ?> y <?php echo $numero2; ?> es <?php echo $mcd; ?>.</p>
  <?php } ?>
  
</body>
</html>
