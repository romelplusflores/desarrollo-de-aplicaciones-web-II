<?php
  $num = isset($_POST['numero']) ? $_POST['numero'] : '';

  $factorial = 1;
  for ($i = 1; $i <= $num; $i++) {
    $factorial *= $i;
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Factorial de un número</title>
</head>
<body>
  <h1>Factorial de un número</h1>
  <form method="post">
    <label for="numero">Ingrese un número:</label>
    <input type="number" name="numero" id="numero" value="<?php echo $num; ?>">
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($num != '') { ?>
    <p>El factorial de <?php echo $num; ?> es: <?php echo $factorial; ?></p>
  <?php } ?>
  
</body>
</html>
