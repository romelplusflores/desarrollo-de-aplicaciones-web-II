<?php
  $numero = isset($_POST['numero']) ? $_POST['numero'] : '';
  $digito = isset($_POST['digito']) ? $_POST['digito'] : '';

  $cadena = strval($numero);

  $encontrado = false;
  for ($i = 0; $i < strlen($cadena); $i++) {
    if ($cadena[$i] == $digito) {
      $encontrado = true;
      break;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Buscar un dígito en un número</title>
</head>
<body>
  <h1>Buscar un dígito en un número</h1>
  <form method="post">
    <label for="numero">Ingrese un número:</label>
    <input type="number" name="numero" id="numero" value="<?php echo $numero; ?>"><br>
    <label for="digito">Ingrese el dígito a buscar:</label>
    <input type="number" name="digito" id="digito" value="<?php echo $digito; ?>"><br>
    <button type="submit">Buscar</button>
  </form>
  
  <?php if ($numero != '' && $digito != '') { ?>
    <?php if ($encontrado) { ?>
      <p>El dígito <?php echo $digito; ?> se encuentra en el número <?php echo $numero; ?>.</p>
    <?php } else { ?>
      <p>El dígito <?php echo $digito; ?> no se encuentra en el número <?php echo $numero; ?>.</p>
    <?php } ?>
  <?php } ?>
  
</body>
</html>
