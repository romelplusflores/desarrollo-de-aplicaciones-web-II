<?php
  $inicio = isset($_POST['inicio']) ? $_POST['inicio'] : '';
  $fin = isset($_POST['fin']) ? $_POST['fin'] : '';

  function es_capicua($numero) {
    $numero_reverso = strrev($numero);
    if ($numero == $numero_reverso) {
      return true;
    } else {
      return false;
    }
  }

  $capicuas = 0;

  for ($i = $inicio; $i <= $fin; $i++) {
    if (es_capicua($i)) {
      $capicuas++;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Cantidad de números capicúa en un rango</title>
</head>
<body>
  <h1>Cantidad de números capicúa en un rango</h1>
  <form method="post">
    <label for="inicio">Ingrese el inicio del rango:</label>
    <input type="number" name="inicio" id="inicio" value="<?php echo $inicio; ?>"><br>
    <label for="fin">Ingrese el fin del rango:</label>
    <input type="number" name="fin" id="fin" value="<?php echo $fin; ?>"><br>
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($inicio != '' && $fin != '') { ?>
    <p>El rango de <?php echo $inicio; ?> a <?php echo $fin; ?> contiene <?php echo $capicuas; ?> números capicúa.</p>
  <?php } ?>
  
</body>
</html>
