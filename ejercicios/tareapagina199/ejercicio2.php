<?php
  $inicio = isset($_POST['inicio']) ? $_POST['inicio'] : '';
  $fin = isset($_POST['fin']) ? $_POST['fin'] : '';

  $pares = 0;
  $impares = 0;

  for ($i = $inicio; $i <= $fin; $i++) {
    if ($i % 5 == 0) {
      continue;
    }

    if ($i % 2 == 0) {
      $pares++;
    } else {
      $impares++;
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Cantidad de números pares e impares en un rango</title>
</head>
<body>
  <h1>Cantidad de números pares e impares en un rango</h1>
  <form method="post">
    <label for="inicio">Ingrese el inicio del rango:</label>
    <input type="number" name="inicio" id="inicio" value="<?php echo $inicio; ?>">
    <label for="fin">Ingrese el fin del rango:</label>
    <input type="number" name="fin" id="fin" value="<?php echo $fin; ?>">
    <button type="submit">Calcular</button>
  </form>
  
  <?php if ($inicio != '' && $fin != '') { ?>
    <p>En el rango <?php echo $inicio; ?> a <?php echo $fin; ?>, hay:</p>
    <ul>
      <li><?php echo $pares; ?> números pares</li>
      <li><?php echo $impares; ?> números impares</li>
    </ul>
  <?php } ?>
  
</body>
</html>
