<?php
if (isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn1"];
    $num1 = 0;
    $num2 = 1;
    $serie = '';
    $cantidad = 0;
    $suma = 0;
    while ($num2 < $n) {
        $serie .= $num2 . ' ';
        $cantidad++;
        $suma += $num2;
        $siguiente = $num1 + $num2;
        $num1 = $num2;
        $num2 = $siguiente;
    }
}
?>

<html>
<head>
    <title>Fibonacci</title>
</head>
<body>
    <link rel="stylesheet" href="estilo.css">
    <form method="post" action="ejercicio02.php">
        <label>Ingrese Numero:</label>
        <input type="text" name="txtn1" value="<?= $n ?>">
        <button type="submit" name="btnCalcular">Calcular</button>
    </form>
    <?php if (!empty($serie)) : ?>
        <p>Serie de Fibonacci: <?= $serie ?></p>
        <p>Cantidad de números en la serie: <?= $cantidad ?></p>
        <p>Suma de los números en la serie: <?= $suma ?></p>
    <?php endif; ?>
</body>
</html>