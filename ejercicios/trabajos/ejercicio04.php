<?php
$numeroObjetos = 0;
$numeroPermutaciones = 0;

if (isset($_POST["btnCalcular"])) {
    $numeroObjetos = (int)$_POST["txtn1"];
    $numeroPermutaciones = 1;
    for ($i = 1; $i <= $numeroObjetos; $i++) {
        $numeroPermutaciones *= $i;
    }
}
?>

<html>

<head>
    <title>Número de permutaciones</title>
    <style type="text/css">
        .TextoFondo {
            background-color: yellow;
        }
    </style>
</head>

<body>
    <link rel="stylesheet" href="estilo.css">
    <form method="post" action="ejercicio04.php">
        <table width="250" border="10">
            <tr>
                <td colspan="2"><strong>Número de permutaciones</strong></td>
            </tr>
            <tr>
                <td>N° de objetos:</td>
                <td>
                    <input name="txtn1" type="text" id="txtn1" value="<?= $numeroObjetos ?>" />
                </td>
            </tr>
            <tr>
                <td>N° de permutaciones:</td>
                <td>
                    <input name="txtp" type="text" class="TextoFondo" id="txtp" value="<?= $numeroPermutaciones ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>

</html>