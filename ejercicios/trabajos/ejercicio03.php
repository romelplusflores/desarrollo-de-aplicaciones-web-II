<?php

$num_digits = 0;
$divisor = 0;
$multiples = 0;

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["btnCalcular"])) {

    $num_digits = filter_var($_POST["txtn1"], FILTER_VALIDATE_INT, array("options" => array("min_range" => 1)));
    $divisor = filter_var($_POST["txtn2"], FILTER_VALIDATE_INT, array("options" => array("min_range" => 1)));

    $smallest_multiple = ceil(pow(10, $num_digits - 1) / $divisor) * $divisor;
    $largest_multiple = floor(pow(10, $num_digits) / $divisor) * $divisor - 1;
    $multiples = max(0, ($largest_multiple - $smallest_multiple) / $divisor + 1);
}

?>

<html>
<head>
    <title>Cantidad múltiplos del divisor</title>
</head>
<body>
    <link rel="stylesheet" href="estilo.css">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <label>cantidad de cifras:</label>
        <input type="number" name="txtn1" min="1" value="<?php echo htmlspecialchars($num_digits); ?>">
        <br>
        <label>Ingrese divisor:</label>
        <input type="number" name="txtn2" min="1" value="<?php echo htmlspecialchars($divisor); ?>">
        <br>
        <button type="submit" name="btnCalcular">Calcular</button>
    </form>
    <?php if ($multiples > 0) : ?>
        <p>Hay <?php echo $multiples; ?> números múltiplos de <?php echo $divisor; ?> con <?php echo $num_digits; ?> cifras.</p>
    <?php endif; ?>
</body>
</html>