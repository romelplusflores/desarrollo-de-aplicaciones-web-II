<!DOCTYPE html>
<html>
  <head>
    <title>Convertir horas a minutos y segundos</title>
  </head>
  <body>
    <h1>Convertir horas a minutos y segundos</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="horas">Ingrese la cantidad de horas:</label>
      <input type="number" name="horas" id="horas"><br><br>
      <input type="submit" value="Convertir">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $horas = $_POST["horas"];
      
      $minutos = $horas * 60;
      $segundos = $horas * 3600;
      
      echo "<h2>Resultados:</h2>";
      echo $horas . " horas son equivalentes a " . $minutos . " minutos y " . $segundos . " segundos.";
    }
    ?>
  </body>
</html>