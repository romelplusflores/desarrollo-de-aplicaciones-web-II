<!DOCTYPE html>
<html>
  <head>
    <title>Calcular área y perímetro de un rectángulo</title>
  </head>
  <body>
    <h1>Calcular área y perímetro de un rectángulo</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="base">Ingrese la base del rectángulo:</label>
      <input type="number" name="base" id="base"><br><br>
      <label for="altura">Ingrese la altura del rectángulo:</label>
      <input type="number" name="altura" id="altura"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $base = $_POST["base"];
      $altura = $_POST["altura"];
      
      $area = $base * $altura;
      $perimetro = 2 * ($base + $altura);
      
      echo "<h2>Resultados:</h2>";
      echo "El área del rectángulo es " . $area . " unidades cuadradas y el perímetro es " . $perimetro . " unidades.";
    }
    ?>
  </body>
</html>
