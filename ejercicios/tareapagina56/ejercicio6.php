<!DOCTYPE html>
<html>
  <head>
    <title>Calcular el área y el perímetro de un cuadrado</title>
  </head>
  <body>
    <h1>Calcular el área y el perímetro de un cuadrado</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="lado">Ingrese la medida del lado del cuadrado:</label>
      <input type="number" name="lado" id="lado"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $lado = $_POST["lado"];
      
      $area = $lado * $lado;
      $perimetro = 4 * $lado;
      
      echo "<h2>Resultados:</h2>";
      echo "El área del cuadrado es: " . $area . "<br>";
      echo "El perímetro del cuadrado es: " . $perimetro;
    }
    ?>
  </body>
</html>