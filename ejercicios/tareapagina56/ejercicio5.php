<!DOCTYPE html>
<html>
  <head>
    <title>Calcular porcentaje de 4 números enteros</title>
  </head>
  <body>
    <h1>Calcular porcentaje de 4 números enteros</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="num1">Ingrese el primer número:</label>
      <input type="number" name="num1" id="num1"><br><br>
      <label for="num2">Ingrese el segundo número:</label>
      <input type="number" name="num2" id="num2"><br><br>
      <label for="num3">Ingrese el tercer número:</label>
      <input type="number" name="num3" id="num3"><br><br>
      <label for="num4">Ingrese el cuarto número:</label>
      <input type="number" name="num4" id="num4"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $num1 = $_POST["num1"];
      $num2 = $_POST["num2"];
      $num3 = $_POST["num3"];
      $num4 = $_POST["num4"];
      
      $suma = $num1 + $num2 + $num3 + $num4;
      
      $porcentaje1 = ($num1 / $suma) * 100;
      $porcentaje2 = ($num2 / $suma) * 100;
      $porcentaje3 = ($num3 / $suma) * 100;
      $porcentaje4 = ($num4 / $suma) * 100;
      
      echo "<h2>Resultados:</h2>";
      echo "El porcentaje del primer número es: " . round($porcentaje1, 2) . "%<br>";
      echo "El porcentaje del segundo número es: " . round($porcentaje2, 2) . "%<br>";
      echo "El porcentaje del tercer número es: " . round($porcentaje3, 2) . "%<br>";
      echo "El porcentaje del cuarto número es: " . round($porcentaje4, 2) . "%";
    }
    ?>
  </body>
</html>