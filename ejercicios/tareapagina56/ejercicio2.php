<!DOCTYPE html>
<html>
  <head>
    <title>Números enteros incluidos</title>
  </head>
  <body>
    <h1>Números enteros incluidos</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="a">Ingrese el valor de a:</label>
      <input type="text" name="a" id="a"><br><br>
      <label for="b">Ingrese el valor de b:</label>
      <input type="text" name="b" id="b"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $a = $_POST["a"];
      $b = $_POST["b"];
      
      $inicio = min($a, $b);
      $fin = max($a, $b);
      $cantidad = $fin - $inicio + 1;
      
      echo "<h2>Resultados:</h2>";
      echo "Entre " . $a . " y " . $b . " hay " . $cantidad . " números enteros incluidos.";
    }
    ?>
  </body>
</html>