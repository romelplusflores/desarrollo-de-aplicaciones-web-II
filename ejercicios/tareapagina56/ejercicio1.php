<!DOCTYPE html>
<html>
  <head>
    <title>Suma y resta de dos números enteros</title>
  </head>
  <body>
    <h1>Suma y resta de dos números enteros</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="a">Ingrese el valor de a:</label>
      <input type="text" name="a" id="a"><br><br>
      <label for="b">Ingrese el valor de b:</label>
      <input type="text" name="b" id="b"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      // Procesar los valores ingresados
      $a = $_POST["a"];
      $b = $_POST["b"];
      
      // Realizar las operaciones
      $suma = $a + $b;
      $resta = $a - $b;
      
      // Mostrar los resultados
      echo "<h2>Resultados:</h2>";
      echo "La suma de a y b es: " . $suma . "<br>";
      echo "La resta de a y b es: " . $resta . "<br>";
    }
    ?>
  </body>
</html>
