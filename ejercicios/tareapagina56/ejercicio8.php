<!DOCTYPE html>
<html>
  <head>
    <title>Convertir grados Fahrenheit a Celsius y Kelvin</title>
  </head>
  <body>
    <h1>Convertir grados Fahrenheit a Celsius y Kelvin</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="fahrenheit">Ingrese la cantidad de grados Fahrenheit:</label>
      <input type="number" name="fahrenheit" id="fahrenheit"><br><br>
      <input type="submit" value="Convertir">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $fahrenheit = $_POST["fahrenheit"];
      
      $celsius = round(($fahrenheit - 32) * 5/9, 2);
      $kelvin = round(($fahrenheit + 459.67) * 5/9, 2);
      
      echo "<h2>Resultados:</h2>";
      echo $fahrenheit . " grados Fahrenheit son equivalentes a " . $celsius . " grados Celsius y " . $kelvin . " grados Kelvin.";
    }
    ?>
  </body>
</html>