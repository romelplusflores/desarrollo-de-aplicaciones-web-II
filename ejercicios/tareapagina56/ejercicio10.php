<!DOCTYPE html>
<html>
  <head>
    <title>Convertir grados sexagesimales a centesimales</title>
  </head>
  <body>
    <h1>Convertir grados sexagesimales a centesimales</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="grados">Ingrese la medida en grados sexagesimales:</label>
      <input type="number" name="grados" id="grados"><br><br>
      <input type="submit" value="Convertir">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $grados = $_POST["grados"];
      
      $centesimales = $grados * 100 / 90;
      
      echo "<h2>Resultado:</h2>";
      echo $grados . " grados sexagesimales equivalen a " . $centesimales . " grados centesimales.";
    }
    ?>
  </body>
</html>