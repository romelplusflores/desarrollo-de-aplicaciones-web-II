<!DOCTYPE html>
<html>
  <head>
    <title>Convertir milímetros a metros, decímetros, centímetros y milímetros</title>
  </head>
  <body>
    <h1>Convertir milímetros a metros, decímetros, centímetros y milímetros</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="milimetros">Ingrese la cantidad de milímetros:</label>
      <input type="text" name="milimetros" id="milimetros"><br><br>
      <input type="submit" value="Convertir">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      // Procesar el valor ingresado
      $milimetros = $_POST["milimetros"];
      
      // Convertir milímetros a metros, decímetros, centímetros y milímetros
      $metros = floor($milimetros / 1000);
      $decimetros = floor(($milimetros % 1000) / 100);
      $centimetros = floor(($milimetros % 100) / 10);
      $milimetros_resto = $milimetros % 10;
      
      // Mostrar el resultado
      echo "<h2>Resultados:</h2>";
      echo $milimetros . " milímetros equivalen a " . $metros . " metros, " . $decimetros . " decímetros, " . $centimetros . " centímetros y " . $milimetros_resto . " milímetros.";
    }
    ?>
  </body>
</html>