<!DOCTYPE html>
<html>
  <head>
    <title>Calcular el doble o triple de un número</title>
  </head>
  <body>
    <h1>Calcular el doble o triple de un número</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="num">Ingrese un número:</label>
      <input type="number" name="num" id="num"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $num = $_POST["num"];
      
      if ($num > 0) {
        $resultado = $num * 2;
      } elseif ($num < 0) {
        $resultado = $num * 3;
      } else {
        $resultado = 0;
      }
      
      echo "<h2>Resultado:</h2>";
      echo "El resultado es " . $resultado;
    }
    ?>
  </body>
</html>
