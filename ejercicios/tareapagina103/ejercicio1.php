<!DOCTYPE html>
<html>
  <head>
    <title>Determinar si una persona es mayor o menor de edad</title>
  </head>
  <body>
    <h1>Determinar si una persona es mayor o menor de edad</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="edad">Ingrese su edad:</label>
      <input type="number" name="edad" id="edad"><br><br>
      <input type="submit" value="Determinar">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $edad = $_POST["edad"];
      
      if ($edad >= 18) {
        $mensaje = "Eres mayor de edad";
      } else {
        $mensaje = "Eres menor de edad";
      }
      
      echo "<h2>Resultado:</h2>";
      echo $mensaje;
    }
    ?>
  </body>
</html>
