<!DOCTYPE html>
<html>
<head>
	<title>Tipo de triángulo</title>
</head>
<body>
	<form method="post">
		<label for="lado1">Lado 1:</label>
		<input type="number" name="lado1"><br><br>

		<label for="lado2">Lado 2:</label>
		<input type="number" name="lado2"><br><br>

		<label for="lado3">Lado 3:</label>
		<input type="number" name="lado3"><br><br>

		<input type="submit" value="Determinar tipo de triángulo">
	</form>

	<?php
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$lado1 = $_POST['lado1'];
			$lado2 = $_POST['lado2'];
			$lado3 = $_POST['lado3'];

			if ($lado1 == $lado2 && $lado1 == $lado3) {
				echo "<p>Tipo de triángulo: Equilátero</p>";
			} elseif ($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3) {
				echo "<p>Tipo de triángulo: Isósceles</p>";
			} else {
				echo "<p>Tipo de triángulo: Escaleno</p>";
			}
		}
	?>
</body>
</html>
