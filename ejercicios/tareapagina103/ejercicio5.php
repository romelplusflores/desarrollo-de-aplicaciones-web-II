<!DOCTYPE html>
<html>
  <head>
    <title>Ordenar tres números</title>
  </head>
  <body>
    <h1>Ordenar tres números</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="num1">Ingrese el primer número:</label>
      <input type="number" name="num1" id="num1"><br><br>
      <label for="num2">Ingrese el segundo número:</label>
      <input type="number" name="num2" id="num2"><br><br>
      <label for="num3">Ingrese el tercer número:</label>
      <input type="number" name="num3" id="num3"><br><br>
      <input type="submit" value="Ordenar">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $num1 = $_POST["num1"];
      $num2 = $_POST["num2"];
      $num3 = $_POST["num3"];
      
      $ascendente = [$num1, $num2, $num3];
      sort($ascendente);
      
      $descendente = [$num1, $num2, $num3];
      rsort($descendente);
      
      echo "<h2>Números ordenados en forma ascendente:</h2>";
      foreach ($ascendente as $num) {
        echo $num . " ";
      }
      
      echo "<h2>Números ordenados en forma descendente:</h2>";
      foreach ($descendente as $num) {
        echo $num . " ";
      }
    }
    ?>
  </body>
</html>
