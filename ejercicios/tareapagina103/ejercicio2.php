<!DOCTYPE html>
<html>
  <head>
    <title>Devolver el número menor entre dos números enteros</title>
  </head>
  <body>
    <h1>Devolver el número menor entre dos números enteros</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="num1">Ingrese el primer número:</label>
      <input type="number" name="num1" id="num1"><br><br>
      <label for="num2">Ingrese el segundo número:</label>
      <input type="number" name="num2" id="num2"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $num1 = $_POST["num1"];
      $num2 = $_POST["num2"];
      
      if ($num1 < $num2) {
        $menor = $num1;
      } else {
        $menor = $num2;
      }
      
      echo "<h2>Resultado:</h2>";
      echo "El número menor es: " . $menor;
    }
    ?>
  </body>
</html>
