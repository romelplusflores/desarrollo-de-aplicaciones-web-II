<?php
$saldo_anterior = $_POST["saldo_anterior"]; 
$tipo_movimiento = $_POST["tipo_movimiento"]; 
$monto_transaccion = $_POST["monto_transaccion"]; 

if ($tipo_movimiento == "D") { 
    $saldo_actual = $saldo_anterior + $monto_transaccion; 
} elseif ($tipo_movimiento == "R") { 
    $saldo_actual = $saldo_anterior - $monto_transaccion; 
}


echo "El saldo actual es: " . $saldo_actual;
?>
<form method="post" action="ejercicio7.php">
    <label for="saldo_anterior">Saldo anterior:</label>
    <input type="number" name="saldo_anterior" id="saldo_anterior" required>
    <br>
    <label for="tipo_movimiento">Tipo de movimiento:</label>
    <select name="tipo_movimiento" id="tipo_movimiento" required>
        <option value="D">Depósito</option>
        <option value="R">Retiro</option>
    </select>
    <br>
    <label for="monto_transaccion">Monto de la transacción:</label>
    <input type="number" name="monto_transaccion" id="monto_transaccion" required>
    <br>
    <input type="submit" value="Calcular saldo">
</form>

