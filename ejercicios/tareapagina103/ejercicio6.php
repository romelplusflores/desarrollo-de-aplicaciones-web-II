<?php
if(isset($_POST['submit'])) {
    // Obtener las notas ingresadas
    $nota1 = $_POST['nota1'];
    $nota2 = $_POST['nota2'];
    $nota3 = $_POST['nota3'];
    $nota4 = $_POST['nota4'];

    $notas = array($nota1, $nota2, $nota3, $nota4);

    rsort($notas);

    $promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;

    if($promedio >= 11) {
        echo "Aprobado";
    } else {
        echo "Desaprobado";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Promedio de Notas</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Nota 1: <input type="number" name="nota1" required><br>
        Nota 2: <input type="number" name="nota2" required><br>
        Nota 3: <input type="number" name="nota3" required><br>
        Nota 4: <input type="number" name="nota4" required><br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
