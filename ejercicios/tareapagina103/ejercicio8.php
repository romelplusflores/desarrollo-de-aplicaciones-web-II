<!DOCTYPE html>
<html>
<head>
	<title>Determinar el número mayor entre a y b</title>
</head>
<body>
	<?php
		$a = 7;
		$b = 5;

		if ($a > $b) {
			echo "a es mayor que b";
		} elseif ($b > $a) {
			echo "b es mayor que a";
		} else {
			echo "a es igual a b";
		}
	?>
</body>
</html>
