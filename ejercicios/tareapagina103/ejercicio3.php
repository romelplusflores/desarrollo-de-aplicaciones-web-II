<!DOCTYPE html>
<html>
  <head>
    <title>Determinar si dos números son iguales o diferentes</title>
  </head>
  <body>
    <h1>Determinar si dos números son iguales o diferentes</h1>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <label for="num1">Ingrese el primer número:</label>
      <input type="number" name="num1" id="num1"><br><br>
      <label for="num2">Ingrese el segundo número:</label>
      <input type="number" name="num2" id="num2"><br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $num1 = $_POST["num1"];
      $num2 = $_POST["num2"];
      
      if ($num1 == $num2) {
        $resultado = "iguales";
      } else {
        $resultado = "diferentes";
      }
      
      echo "<h2>Resultado:</h2>";
      echo "Los números ingresados son " . $resultado;
    }
    ?>
  </body>
</html>
