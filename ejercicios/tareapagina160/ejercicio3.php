<?php
$operador = "+";

switch ($operador) {
  case "+":
    echo "Suma";
    break;
  case "-":
    echo "Resta";
    break;
  case "*":
    echo "Multiplicación";
    break;
  case "/":
    echo "División";
    break;
  default:
    echo "Operador inválido";
    break;
}
?>
