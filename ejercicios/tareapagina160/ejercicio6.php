<!DOCTYPE html>
<html>
<head>
	<title>Frutería</title>
</head>
<body>
	<h1>Frutería</h1>
	<form method="post">
		<label for="kilos">Ingrese la cantidad de kilos de manzanas:</label>
		<input type="number" id="kilos" name="kilos" min="0" step="0.01" required>
		<button type="submit">Calcular precio</button>
	</form>

	<?php
		if(isset($_POST['kilos'])) {
			$kilos = $_POST['kilos'];
			$descuento = 0;

			if($kilos > 0 && $kilos <= 2) {
				$descuento = 0;
			} elseif($kilos > 2 && $kilos <= 5) {
				$descuento = 10;
			} elseif($kilos > 5 && $kilos <= 10) {
				$descuento = 20;
			} elseif($kilos > 10) {
				$descuento = 30;
			}

			$precio = $kilos * 2.5; // El precio por kilo es de $2.5
			$descuento_monto = ($precio * $descuento) / 100;
			$total = $precio - $descuento_monto;

			echo "<p>Usted compró $kilos kilos de manzanas.</p>";
			echo "<p>El precio por kilo es de $2.5.</p>";
			echo "<p>El descuento aplicado es del $descuento%.</p>";
			echo "<p>El monto del descuento es de $$descuento_monto.</p>";
			echo "<p>El total a pagar es de $$total.</p>";
		}
	?>
</body>
</html>
