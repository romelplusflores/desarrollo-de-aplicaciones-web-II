<?php

$sueldo = $_POST['sueldo'];
$sexo = $_POST['sexo'];
$tarjeta = $_POST['tarjeta'];

if ($tarjeta == "Obrero") {
    if ($sexo == "Hombre") {
        $descuento = $sueldo * 0.15;
    } else {
        $descuento = $sueldo * 0.10;
    }
} else {
    if ($sexo == "Hombre") {
        $descuento = $sueldo * 0.20;
    } else {
        $descuento = $sueldo * 0.15;
    }
}

$sueldo_con_descuento = $sueldo - $descuento;

echo "El descuento aplicado es de $" . $descuento . "<br>";
echo "El sueldo final es de $" . $sueldo_con_descuento;
?>
