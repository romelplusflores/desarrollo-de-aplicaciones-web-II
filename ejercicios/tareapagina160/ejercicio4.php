<?php
$numero_canal = 5; // Número del canal a buscar

// Array asociativo que asocia números de canales con nombres de canales
$canales = array(
    2 => "América TV",
    4 => "Latina",
    5 => "Panamericana TV",
    9 => "ATV",
    13 => "Willax TV"
);

if (array_key_exists($numero_canal, $canales)) { // Verificar si el número de canal existe en el array
    $nombre_canal = $canales[$numero_canal];
    echo "El canal con número $numero_canal es $nombre_canal.";
} else {
    echo "No se encontró un canal con número $numero_canal.";
}
?>
