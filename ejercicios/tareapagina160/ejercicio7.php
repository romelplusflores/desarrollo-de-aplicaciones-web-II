<?php
$codigo = 1;

if ($codigo == 0) {
  $estadoCivil = 'Soltero';
} elseif ($codigo == 1) {
  $estadoCivil = 'Casado';
} elseif ($codigo == 2) {
  $estadoCivil = 'Divorciado';
} elseif ($codigo == 3) {
  $estadoCivil = 'Viudo';
} else {
  $estadoCivil = 'Código de estado civil inválido';
}

echo "El código $codigo corresponde al estado civil de $estadoCivil";
?>
