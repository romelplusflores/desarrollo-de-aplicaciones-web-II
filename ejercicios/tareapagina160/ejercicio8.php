<?php
$tiempo_servicio = $_POST['tiempo_servicio'];
$cargo = $_POST['cargo'];

if ($tiempo_servicio >= 0 && $tiempo_servicio <= 2) {
    if ($cargo == 'Administrador') {
        $utilidades = 2000;
    } elseif ($cargo == 'Contador') {
        $utilidades = 1500;
    } elseif ($cargo == 'Empleado') {
        $utilidades = 1000;
    }
} elseif ($tiempo_servicio >= 3 && $tiempo_servicio <= 5) {
    if ($cargo == 'Administrador') {
        $utilidades = 2500;
    } elseif ($cargo == 'Contador') {
        $utilidades = 2000;
    } elseif ($cargo == 'Empleado') {
        $utilidades = 1500;
    }
} elseif ($tiempo_servicio >= 6 && $tiempo_servicio <= 8) {
    if ($cargo == 'Administrador') {
        $utilidades = 3000;
    } elseif ($cargo == 'Contador') {
        $utilidades = 2500;
    } elseif ($cargo == 'Empleado') {
        $utilidades = 2000;
    }
} elseif ($tiempo_servicio > 8) {
    if ($cargo == 'Administrador') {
        $utilidades = 4000;
    } elseif ($cargo == 'Contador') {
        $utilidades = 3500;
    } elseif ($cargo == 'Empleado') {
        $utilidades = 1500;
    }
}

echo "El monto de utilidades para un trabajador con $tiempo_servicio años de servicio y cargo de $cargo es: $utilidades";
?>
