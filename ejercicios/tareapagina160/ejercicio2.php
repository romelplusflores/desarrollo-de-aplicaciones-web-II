<?php
if(isset($_POST['numero'])) {
    $numero = $_POST['numero'];
    switch($numero) {
        case 1:
            echo "Domingo";
            break;
        case 2:
            echo "Lunes";
            break;
        case 3:
            echo "Martes";
            break;
        case 4:
            echo "Miércoles";
            break;
        case 5:
            echo "Jueves";
            break;
        case 6:
            echo "Viernes";
            break;
        case 7:
            echo "Sábado";
            break;
        default:
            echo "Número inválido";
    }
}
?>

<form method="post">
    <label for="numero">Ingresa un número del 1 al 7:</label>
    <input type="number" name="numero">
    <input type="submit" value="Enviar">
</form>
