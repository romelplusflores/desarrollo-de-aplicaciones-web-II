<?php
$num_mes = 5;

$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

if ($num_mes >= 1 && $num_mes <= 12) {
  $nombre_mes = $meses[$num_mes-1];
  echo "El mes correspondiente al número $num_mes es: $nombre_mes";
} else {
  echo "Error: el número de mes ingresado no es válido.";
}
?>
