<?php

date_default_timezone_set('America/Lima');

$hoy = date('Y-m-d');

$ultimo_dia = date('Y-12-31');

$diferencia = round((strtotime($ultimo_dia) - strtotime($hoy)) / (60 * 60 * 24));

echo "Faltan $diferencia días para que acabe el año.";

?>
