<?php
$matriz = array(
    array(5, 10, 15),
    array(20, 25, 30),
    array(35, 40, 45),
    array(50, 55, 60)
);

$mayores = array(0, 0, 0);

for ($i = 0; $i < 4; $i++) {
    for ($j = 0; $j < 3; $j++) {
        if ($matriz[$i][$j] > $mayores[$j]) {
            $mayores[$j] = $matriz[$i][$j];
        }
    }
}

for ($j = 0; $j < 3; $j++) {
    echo "El mayor de la columna $j es: " . $mayores[$j] . "<br>";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>matriz de numeros mayores</title>
</head>
<body>
    <link rel="stylesheet" href="estilos.css">
</body>
</html>