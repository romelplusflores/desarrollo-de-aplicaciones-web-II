<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$numeros = $_POST["numeros"];

			$suma_filas = array();

			for ($i = 0; $i < 3; $i++) {
				$suma = 0;
				for ($j = 0; $j < 2; $j++) {
					$suma += $numeros[$i][$j];
				}
				$suma_filas[] = $suma;
			}

			echo "La suma de cada fila es: " . implode(", ", $suma_filas);
		}
	?>

<!DOCTYPE html>
<html>
<head>
	<title>Matriz</title>
</head>
<body>
    <link rel="stylesheet" href="estilos.css">
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label>Ingrese el número en la posición (1,1):</label>
		<input type="number" name="numeros[0][0]" required><br>
		<label>Ingrese el número en la posición (1,2):</label>
		<input type="number" name="numeros[0][1]" required><br>
		<label>Ingrese el número en la posición (2,1):</label>
		<input type="number" name="numeros[1][0]" required><br>
		<label>Ingrese el número en la posición (2,2):</label>
		<input type="number" name="numeros[1][1]" required><br>
		<label>Ingrese el número en la posición (3,1):</label>
		<input type="number" name="numeros[2][0]" required><br>
		<label>Ingrese el número en la posición (3,2):</label>
		<input type="number" name="numeros[2][1]" required><br>
		<button type="submit">Calcular suma</button>
	</form>
</body>
</html>