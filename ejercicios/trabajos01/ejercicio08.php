<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $letra = $_POST["letra"];
  if (ctype_upper($letra)) {
    echo "La letra ingresada es mayúscula";
  } elseif (ctype_lower($letra)) {
    echo "La letra ingresada es minúscula";
  } else {
    echo "El valor ingresado no es una letra";
  }
}
?>
<!DOCTYPE html>
<html>
<body>
    <link rel="stylesheet" href="estilos.css">

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Ingrese una letra: <input type="text" name="letra">
  <input type="submit" value="Enviar">
</form>
</body>
</html>