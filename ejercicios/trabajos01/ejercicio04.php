<?php
		$matriz = array(
			array(2, 4, 6),
			array(8, 10, 12)
		);
		$k = 3;

		echo "<h3>Matriz Original:</h3>";
		echo "<table border='1'>";
		foreach ($matriz as $fila) {
			echo "<tr>";
			foreach ($fila as $valor) {
				echo "<td>" . $valor . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";

		for ($i=0; $i<2; $i++) {
			for ($j=0; $j<3; $j++) {
				$matriz[$i][$j] *= $k;
			}
		}

		echo "<h3>Matriz Resultante:</h3>";
		echo "<table border='1'>";
		foreach ($matriz as $fila) {
			echo "<tr>";
			foreach ($fila as $valor) {
				echo "<td>" . $valor . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";

		$suma = 0;
		for ($i=0; $i<2; $i++) {
			for ($j=0; $j<3; $j++) {
				$suma += $matriz[$i][$j];
			}
		}

		echo "<h3>Suma de los valores de la matriz resultante: " . $suma . "</h3>";
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Operaciones con Matrices en PHP</title>
</head>
<body>
    <link rel="stylesheet" href="estilos.css">
</body>
</html>
