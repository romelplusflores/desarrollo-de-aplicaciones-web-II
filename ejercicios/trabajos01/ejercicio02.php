<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$numeros = $_POST["numeros"];
			$forma = $_POST["forma"];

			if ($forma == "A") {
				sort($numeros);
			} else if ($forma == "D") {
				rsort($numeros);
			}

			echo "Los números ordenados en forma " . ($forma == "A" ? "ascendente" : "descendente") . " son: " . implode(", ", $numeros);
		}
	?>

<!DOCTYPE html>
<html>
<head>
	<title>Números</title>
</head>
<body>
    <link rel="stylesheet" href="estilos.css">
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label>Ingrese el primer número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el segundo número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el tercer número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el cuarto número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el quinto número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ordenar en forma:</label>
		<select name="forma">
			<option value="A">Ascendente</option>
			<option value="D">Descendente</option>
		</select>
		<button type="submit">Ordenar</button>
	</form>
</body>
</html>