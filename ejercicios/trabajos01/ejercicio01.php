<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$numeros = $_POST["numeros"];

			$mayor = max($numeros);

			$menor = min($numeros);

			echo "El número mayor es: " . $mayor . "<br>";
			echo "El número menor es: " . $menor . "<br>";
		}
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Números</title>
</head>
<body>
    <link rel="stylesheet" href="estilos.php">
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label>Ingrese el primer número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el segundo número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el tercer número:</label>
		<input type="number" name="numeros[]" required><br>
		<label>Ingrese el cuarto número:</label>
		<input type="number" name="numeros[]" required><br>
		<button type="submit">Calcular</button>
	</form>

</body>
</html>