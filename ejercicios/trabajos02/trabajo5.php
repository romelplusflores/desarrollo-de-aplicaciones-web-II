<?php
function multiplicar_matrices($A, $B) {
    $C = array();
    for ($i = 0; $i < 2; $i++) {
        for ($j = 0; $j < 2; $j++) {
            $C[$i][$j] = 0;
            for ($k = 0; $k < 2; $k++) {
                $C[$i][$j] += $A[$i][$k] * $B[$k][$j];
            }
        }
    }
    return $C;
}

$A = array(
    array(1, 2),
    array(3, 4)
);

$B = array(
    array(5, 6),
    array(7, 8)
);

$C = multiplicar_matrices($A, $B);

echo "<table>";
for ($i = 0; $i < 2; $i++) {
    echo "<tr>";
    for ($j = 0; $j < 2; $j++) {
        echo "<td>" . $C[$i][$j] . "</td>";
    }
    echo "</tr>";
}
echo "</table>";
?>