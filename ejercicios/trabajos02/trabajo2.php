<?php
function SumaParesImpares($numero, &$sumaPares, &$sumaImpares) {
    $sumaPares = 0;
    $sumaImpares = 0;

    $numeroStr = strval($numero);

    for ($i = 0; $i < strlen($numeroStr); $i++) {
        $digito = intval($numeroStr[$i]);

        if ($digito % 2 == 0) {
            $sumaPares += $digito;
        } else {
            $sumaImpares += $digito;
        }
    }
}
?>
<?php
$numero = 123456;
$sumaPares = 0;
$sumaImpares = 0;

SumaParesImpares($numero, $sumaPares, $sumaImpares);

echo "La suma de los dígitos pares de $numero es: $sumaPares <br>";
echo "La suma de los dígitos impares de $numero es: $sumaImpares";
?>
