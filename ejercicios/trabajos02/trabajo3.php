<!DOCTYPE html>
<html>
<head>
	<title>Calcular el área de un rectángulo</title>
</head>
<body>
	<h1>Calcular el área de un rectángulo</h1>
	<form action="trabajo3.php" method="post">
		<label for="base">Base:</label>
		<input type="number" name="base" id="base" required>

		<label for="altura">Altura:</label>
		<input type="number" name="altura" id="altura" required>

		<button type="submit">Calcular</button>
	</form>
</body>
</html>
<?php
function AreaRectangulo(float $Base, float $Altura): float {
    $area = $Base * $Altura;
    return $area;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $base = $_POST["base"];
    $altura = $_POST["altura"];

    $area = AreaRectangulo($base, $altura);

    echo "El área del rectángulo es: " . $area;
}
?>
