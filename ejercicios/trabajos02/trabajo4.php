<?php
function InvertirNumero(int $numero): int {
    $inverso = 0;
    while ($numero > 0) {
        $digito = $numero % 10;
        $inverso = $inverso * 10 + $digito;
        $numero = (int)($numero / 10);
    }
    return $inverso;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $numero = $_POST["numero"];

    $inverso = InvertirNumero($numero);

    echo "El número invertido es: " . $inverso;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Invertir un número</title>
</head>
<body>
	<h1>Invertir un número</h1>
	<form action="trabajo4.php" method="post">
		<label for="numero">Ingrese un número:</label>
		<input type="number" name="numero" id="numero" required>

		<button type="submit">Invertir</button>
	</form>
</body>
</html>