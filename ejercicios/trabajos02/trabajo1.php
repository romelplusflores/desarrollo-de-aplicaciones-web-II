<?php
function Promedio(float $n1, float $n2, float $n3): float {
    $notas = array($n1, $n2, $n3);
    rsort($notas); // Ordenar de mayor a menor
    $promedio = ($notas[0] + $notas[1]) / 2;
    return $promedio;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $n1 = $_POST["n1"];
    $n2 = $_POST["n2"];
    $n3 = $_POST["n3"];

    $promedio = Promedio($n1, $n2, $n3);

    echo "<h2>El promedio de las dos notas mayores es: $promedio</h2>";
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Calcular promedio de notas</title>
</head>
<body>
	<h1>Calcular promedio de notas</h1>
	<form action="trabajo1.php" method="post">
		<label for="n1">Nota 1:</label>
		<input type="number" name="n1" id="n1" required>
		<br>
		<label for="n2">Nota 2:</label>
		<input type="number" name="n2" id="n2" required>
		<br>
		<label for="n3">Nota 3:</label>
		<input type="number" name="n3" id="n3" required>
		<br>
		<button type="submit">Calcular promedio</button>
	</form>
</body>
</html>